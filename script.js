let myIndex = 0;
slideShow();

function slideShow() {
    let i;
    const x = document.getElementsByClassName("carousel-item");

    for (let i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    myIndex++;
    if (myIndex > x.length) {
        myIndex = 1;
    }
    x[myIndex - 1].style.display = "block";
    setTimeout(slideShow, 3000); // Change image every 2 seconds
}

//Navbar Hamburger Menu (Responsive)
function toggleMenu() {
    const navItem = document.querySelector('.nav-dropdown');
    navItem.classList.toggle('show');
}
